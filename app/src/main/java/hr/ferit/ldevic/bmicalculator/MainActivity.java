package hr.ferit.ldevic.bmicalculator;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity implements View.OnClickListener {
    private Button bCalculate;
    private TextView tvResult , tvTextresult;
    private EditText etEnterheight, etEnterweight;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.initializeUI();
        
    }

    private void initializeUI()
    {
       this.bCalculate= this.findViewById(R.id.bCalculate);
       this.bCalculate.setOnClickListener(this);
       this.tvResult= this.findViewById(R.id.tvResult);
       this.tvTextresult= this.findViewById(R.id.tvTextresult);
       this.etEnterheight= this.findViewById(R.id.etEnterheight);
       this.etEnterweight= this.findViewById(R.id.etEnterweight);

    }

    @Override
    public void onClick(View view) {
        String sheight = this.etEnterheight.getText().toString();
        String sweight = this.etEnterweight.getText().toString();
        double height = Double.parseDouble(sheight);
        double weight = Double.parseDouble(sweight);
        if (height > 2.5 || height <= 0) {
            this.tvResult.setText(String.valueOf(0));
        } else if (weight > 350 || weight <= 0) {
            this.tvResult.setText(String.valueOf(0));
        } else if (height < 2.5 || height > 0 && weight < 350 || weight > 0) {
            double height2 = height * height;
            double BMI = weight / height2;
            this.tvResult.setText(String.valueOf(BMI));
            if (BMI < 18.5 && BMI > 0) {
                this.tvTextresult.setText(R.string.Underweight);
            } else if (BMI >= 18.5 && BMI < 24.9) {
                this.tvTextresult.setText(R.string.Normal);
            } else if (BMI >= 25 && BMI < 29.9) {
                this.tvTextresult.setText(R.string.Overweight);
            } else if (BMI >= 30) {
                this.tvTextresult.setText(R.string.Obese);

            }
        }
    }
}
